////////////////////////////////////////////////////////////////////////////////////////////
//	Name: Jose Madureira - CS 291 - PROJECT #2 - Fall 2016
//	Date: 08/10/2016
//	Project: Electronic Store Inventory Management
//	Pseudo Code: (1) fileIO.cpp reads data from a file and puts it inside vvIO vector
//		   		 (2) The fileIO.cpp vector is swapped to another vector inside Xclass
//				 (3) User is present with a menu for database operations
//				 (4) methods.cpp performs desired user operations
//				 (5) After add/remove/quit, user is presented with file save choice
//				 	 (5.1) If user says yes, vvXclass vector is swapped back to fileIO
//				 	 (5.2) If user doesnt want to save, they are brought back to menu
//				 (6) fileIO.cpp saves data to file
//
////////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <iomanip>
#include <cstdlib>
#include "headers.h"
using namespace std;

int main(const int argc, const char* argv[]) {
	const string fn="database.csv";
	string sR="";
	int m1cin=0;

	cout<<"\n\t\t----------------------------------------"
	    <<"\n\t\t\t###Axyd Ware LLC####"
	    <<"\n  Always Follow Protocol, and Report ANY Suspicious Activity to Your Supervisor"
	    <<"\n\n\t\t    MCC Inventory Management v1.3"
	    <<"\n\t\t----------------------------------------";


	/*Read/Write to File*/
	fileIO *io= new fileIO(fn);

	/*The rest of the Methods*/
	Xclass *xc = new Xclass(fn);

	io->MCC('r');	//tell MCC to read file
	io->vvSwap(io->vvIO,xc->vvXclass);	//Swap 2D vector from IO to Xclass for modding
	delete io;

	/*Handles User Interaction*/
	xc->zeMenu();

	/*Save and/or Quit*/
	const char cY[]="yY1";
	string usr="";
	cout<<"   Would You Like to Save to '"<<fn<<"' ? ";
	cin.get();
	getline(cin,usr);

	if(usr[0]==cY[0] || usr[0]==cY[0] || usr[0]==cY[0]) {
		fileIO *fi= new fileIO(fn);
		fi->vvSwap(xc->vvXclass,fi->vvIO);
		fi->MCC('w');	//call for writeFile method
		delete fi;
	}//else break;

	delete xc;

	cout<<"\n\t\tGOODBYE!\n";
	system("pause");
	return 0;
}
