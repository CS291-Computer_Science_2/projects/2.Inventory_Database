///////////////////////////////////////////////////////////////////
//	Name: Jose Madureira - CS 291 - PROJECT #2 - Fall 2016
//	Date: 08/10/2016
//  File Purpose: File Read/Write Methods
///////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>	//split string at comma
#include "headers.h"
using namespace std;

fileIO::fileIO(string dbf) : dbFile(dbf), vvIO(0, vector<string>(0,"")) {};
fileIO::~fileIO() {
	dbFile="";
	vvIO.clear();
};

// Controls wether to read or write to a file
void fileIO::MCC(char rw) {
	cout<<"\n  MCC Status: ";
	if(rw=='r') {
		readFile();
	} else if(rw=='w') {
		writeFile();
		cout<<"finished writing file";
	} else cout<<" \trw value: "<<rw<<endl;
};

// Swaps 2 vectors
void fileIO::vvSwap(vector<vector<string> > &vvFrom, vector<vector<string> > &vvTo) {
	swap(vvFrom, vvTo);
}

// File Read
void fileIO::readFile() {
	ios::sync_with_stdio(false);	//buffered I/O
	string s;

	fstream fs(dbFile);
	cin.clear();
	string tmpCol;

	while( getline(fs, s) ) {	//read whole line 1st
		stringstream ss(s);
		vector<string> vCol;
		while(getline(ss, tmpCol, ',')) {	//split string at comma
			vCol.push_back(tmpCol);
		}
		vvIO.insert(vvIO.end(),vCol);
	}

	fs.close();
	cout<<"finished reading '"<<dbFile<<"'.\n";
};

// File write
void fileIO::writeFile() {
	ios::sync_with_stdio(false);
	ofstream of;
	of.open(dbFile);

	int ktr=0, rows=vvIO.size();
	for(auto it : vvIO) {
		++ktr;
		if(ktr == rows) {
			of <<it[0]<<","<<it[1]<<","<<it[2]<<","<<it[3];
			break;
		}
		of <<it[0]<<","<<it[1]<<","<<it[2]<<","<<it[3]<<endl;
	}
	of.close();
};

