#ifndef HEADERS_H
#define HEADERS_H
///////////////////////////////////////////////////////////////////
//	Name: Jose Madureira - CS 291 - PROJECT #2 - Fall 2016
//	Date: 08/10/2016
//  File Purpose: Holds Function Prototypes and Error Messages
///////////////////////////////////////////////////////////////////
#include <string>
#include <vector>
using namespace std;

// Pre-defined error messages
template <class SB> SB switchBlade (const SB &in) {	//instantiated by Xclass::errMacGyver
	switch(in) {
		case 1:
			cout<<"\n\t\t/!\\ Negative Number /!\\";
			break;
		case 2:
			cout<<"\n\t\t/!\\ Too Small /!\\";
			break;
		case 3:
			cout<<"\n\t\t/!\\ Too Large /!\\";
			break;
//		/*UNUSUAL ERRORS*/
		case 111:
			cout<<"\n\t\t/!\\ Invalid Option Selected /!\\\n";
			break;
		case 222:
			cout<<"\n\t\t/!\\ MCC received neither 'r' nor 'w' /!\\";
			break;
		case 444:
			cout<<"\n\t\t/!\\ Zapper is Broken /!\\";
			break;
		case 555:
			cout<<"\n\t\t/!\\ This Sku Already Exists /!\\";
			break;
		default:
			break;
	}
}

// File Operations
class fileIO {
	public:
		fileIO(string);
		~fileIO();

		void MCC(char);
		void readFile();
		void writeFile();
		vector<vector<string> > vvIO;
		void vvSwap(vector<vector<string> > &, vector<vector<string> > &);
	private:
		string dbFile;
};

// Handles User Demanded Operations
class Xclass {
	public:
		Xclass(const string);
		~Xclass();

		void zeMenu();
		void itemRoster();
		void addItem();
		void delItem();
		void itemInfo();
		void sortedList();
		vector<vector<string> > vvXclass;

	private:
		vector<int> s2i(string&);
		void zapper(int*,string&);
		void printItem(const int&, const vector<string>*);
		void cleanCin(); //fixes cin errors
		void checkOptions(); //
		void errMacGyver(int,int,double,int,int);	//error handler for numeric/invalid input
		const string theFile;
		int choice;

};

#endif
