///////////////////////////////////////////////////////////////////
//	Name: Jose Madureira - CS 291 - PROJECT #2 - Fall 2016
//	Date: 08/10/2016
//  File Purpose: Holds methods for all operations, other than file read/write
///////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm>
#include <utility>
#include <cctype>	//lexographical compare
#include <sstream>
#include "math.h"	//ceil for float precision
#include "headers.h"
using namespace std;

Xclass::Xclass(const string file) : theFile(file), vvXclass(0, vector<string>(0,"")) {
	choice=0;
};
Xclass::~Xclass(){
	choice=0;
	vvXclass.clear();
};

// Main Menu
void Xclass::zeMenu() {
	int ch;
	string sR="";
	const char cY[]="yY1";
	do {
		cout<<"\n\t\t.:Main Menu:.";
		cout<<"\n\t//   (1) View Full Inventory"<<"\n\t//   (2) Search for Item"
		    <<"\n\t\\\\   (3) Add Item"<<"\n\t//   (4) Remove Item"<<"\n\t\\\\   (5) Sorted List"
		    <<"\n\t//   (6) Save File and Quit\n";

		do  {
			cout<<">>You Choose: ";
			cin >> ch;
			choice= ch; //choice is Xclass private variable

			errMacGyver(0,0,0.0,ch,111);
		} while(ch<1 || ch>6 || cin.fail());

		/*Clear screen upon leaving*/
		if(ch==6) {
			system("cls");
			break;
		}

		checkOptions();
		sR="y";
	} while(sR[0]==cY[0] || sR[0]==cY[1] || sR[0]==cY[2]);	//Check first letter of input for (y,Y,1)
};

// Executes proper menu action
void Xclass::checkOptions() {
	int *select= &choice;
	switch (*select) {
		case 1:
			cout<<"\n..Viewing Full inventory.\n";
			itemRoster();
			cin.get();
			break;
		case 2:
			cout<<"\n..Display Information for an Item.\n";
			cin.get();
			itemInfo();
			break;
		case 3:
			cout<<"\n..Add New Item (please use '-' in place of ',').\n";
			addItem();
			break;
		case 4:
			cout<<"\n..Remove an Item From the Inventory.\n";
			delItem();
			break;
		case 5:
			cout<<"\n..Display Sorted Inventory.\n";
			sortedList();
			itemRoster();
			cin.get();
			break;
		default:
			cout<<"\n\t\t/!\\Invalid Menu Selection/!\\\n";
	}
};

// Full inventory list
void Xclass::itemRoster() {
	int rows = vvXclass.size();
	int cols = vvXclass[0].size();
	cout<<"\n\tNumber of items: "<<rows-1<<'\n';

	for(int a=0; a<rows; ++a ) {
		if(a==1) cout<<setw(72)<<setfill('-')<<"-\n";	//max 82
		for(const auto &sup : vvXclass[a]) {
			cout << sup;
		}
		cout << '\n';
	}
	cout << "\n";
};

// Search for item
void Xclass::itemInfo() {
	string sin;
	int srch;

	do  {
		cout<<"\nEnter SKU, or 'q' to quit: ";
		getline(cin,sin);
		char qq='q';
		if(sin[0]==qq) return;

		int zar[2]= {1,0};
		zapper(zar,sin);
		istringstream (sin) >> srch;

		errMacGyver(srch,0,0.0,0,0);
		if(srch>=10000000 && srch<=99999999) break;
	} while(srch<10000000 || srch>99999999 || cin.fail());

	int ctr=0, rows= vvXclass.size();
	for(auto it: vvXclass) {
		ctr++;
		if (ctr > rows) {
			printf("\nSKU: %d not found.\n", srch);
			break;
		}
		string hold= it[0];
		int j[2] {1,0};
		zapper(j, hold); //strip formatting

		if(sin== hold) {
			printf("FOUND IT: ");
			printItem(ctr, &it);
			return;
		}
	}
};

//Display item
void Xclass::printItem(const int& ctr, const vector<string> *p) {
	string _s= p->at(0),
	       _q= p->at(1),
	       _p= p->at(2),
	       _d= p->at(3);

	printf("\n %-*s|%-*s|%-*s|%-*s\n",9,"   SKU", 5,"UNITS", 8,"PRICE",30, "ITEM DESCRIPTION");
	cout<<setw(10)<<setfill('-')<<"|"<<setw(6)<<"|"<<setw(9)<<"|"<<setw(53)<<"-"<<endl;	//max 82
	printf("%-*s|%-*s|%-*s|%-*s\n",8,_s.c_str(), 5,_q.c_str(), 8,_p.c_str(),55, _d.c_str());
};

/*TRIMS INVALID CHARACTERS*/
void Xclass::zapper(int *alf, string& zap) { 
	string trim;
	if (alf[0]==1) trim = "..  \n\t";	//for int
	else if(alf[1]==1) trim = "  \n\t";	//for float
	else {
		switchBlade(444);
		return;
	}
	int i {};
	while ((i = zap.find_first_of(trim)) != -1) zap.erase(i,1);
};

// Adds item to database
void Xclass::addItem() {
	cin.get();
	ios::sync_with_stdio(false);	//buffered I/O
	string asku, aqnt, aprc, desc;
	int qn= 777, sk= 1;
	double prc= 0.01f;

	cout<<"If you wish to quit, type 'q' in any field.\n";
	char qq='q';
	do  {
		cout<<"....8 Digit SKU (10000000 to 99999999): ";
		int zar[2]= {1,0};
		getline(cin,asku);
		if(asku[0]==qq) return;

		zapper(zar,asku);
		istringstream (asku) >> sk;
		errMacGyver(sk,0,0.0,0,0);

	} while(sk<10000000 || sk>99999999 || cin.fail());

	cout<<"\nNEVER HOLD MORE THAN 99 UNITS IN STOCK, PER COMPANY DIRECTIVE 33.6(b)";
	do  {
		cout<<"\n....Quantity: ";
		int zar[2]= {1,0};
		getline(cin,aqnt);
		if(aqnt[0]==qq) return;

		zapper(zar,aqnt);
		istringstream (aqnt) >> qn;

		errMacGyver(0,qn,0.0,0,0);
	} while(qn<0 || qn>99);

	cout<<"\nNO ITEM SHALL BE WORTH OVER $20k, PER COMPANY DIRECTIVE 33.6(c)";
	do {
		cout<<"\n....Price(NO '$' SIGN): ";
		int zar[2]= {0,1};
		getline(cin,aprc);
		if(aprc[0]==qq) return;

		zapper(zar,aprc);
		istringstream (aprc) >> prc;

		double scale = 0.01;
		prc = ceil(prc / scale) * scale;	//round up to hundreds
		stringstream ss;
		ss << prc;
		aprc= ss.str();
		errMacGyver(0,0,prc,0,0);
	} while(prc<0.50 || prc>=20000.01);
	cout<<"....Description: ";
	getline(cin, desc);
	if(desc.length()==1 && desc[0]==qq) return;


	vector<string> vtmp;
	vtmp.insert(vtmp.end(), {asku,aqnt,aprc,desc});	//way less verbose than multiple push_back
	vector<string> vp;
	cout<<endl;
	int xctr=0;
	for(auto fz: vtmp) {
		++xctr;
		stringstream ss;
		switch(xctr) {
			case 1:
				ss <<left<<setw(9)<<fz;
				break;
			case 2:
				ss <<left<<setw(3)<<fz;
				break;
			case 3:
				ss <<left<<setw(8)<<fz;
				break;
			case 4:
				ss <<desc;
		}
		vp.push_back(ss.str());
		cout<<" "<<ss.str();
	}
	cout<<endl;

	vtmp.clear();
	vvXclass.insert(vvXclass.end(), {vp});

	const char cY[]="yY1";
	string usr="";
	cout<<"   Would You Like to Save to '"<<theFile<<"' ? ";
	getline(cin,usr);

	if(usr[0]==cY[0] || usr[0]==cY[0] || usr[0]==cY[0]) {
		fileIO *fi= new fileIO(theFile);
		fi->vvSwap(vvXclass,fi->vvIO);
		fi->MCC('w');	//call for writeFile method
		fi->vvSwap(fi->vvIO, vvXclass);
		delete fi;
	}
};

// Removes item from database
void Xclass::delItem() {
	cout<<"....Enter Item SKU Number, or 'q' to quit: ";
	cin.get();
	string ds;
	getline(cin, ds);
	if("q"==&ds[0]) return;

	int _sku, zar[2] {1,0};
	zapper(zar,ds);
	istringstream (ds) >> _sku;

	errMacGyver(_sku,0,0.0,0,0);
	int ctr=0, rows= vvXclass.size();
	for(auto it: vvXclass) {
		if (ctr == rows) {
			cout<<" '"<<_sku<<"'  was not found in the file '"<<theFile<<"' "<<endl;
			break;
		}
		string hold= it[0];
		zapper(zar, hold); //strip formatting

		if(ds==hold) {
			cout<<"\nThe following SKU has been deleted.\n";
			printItem(ctr, &it);
			vvXclass.erase(remove(vvXclass.begin(), vvXclass.end(), vvXclass[ctr]), vvXclass.end());
			
			const char cY[]="yY1";
			string usr="";
			cout<<"   Would You Like to Save Changes to '"<<theFile<<"' ? ";
			getline(cin,usr);

			if(usr[0]==cY[0] || usr[0]==cY[0] || usr[0]==cY[0]) {
				fileIO *fi= new fileIO(theFile);
				fi->vvSwap(vvXclass,fi->vvIO);
				fi->MCC('w');	//call for writeFile method
				fi->vvSwap(fi->vvIO, vvXclass);
				delete fi;
			}
			return;
		}
		++ctr;
	}
};


// Sorts database items
void Xclass::sortedList() {
	sort(vvXclass.begin(), vvXclass.end());
};


// Fix cin errors
void Xclass::cleanCin() {
	cout<<"\n\t\t/!\\INVALID INPUT/!\\\n";
	cin.clear();
	cin.get();
	fflush(stdin);
};

// Check for proper data input format, and ranges
void Xclass::errMacGyver(int sku, int qty, double price, int menu, int CASE) {
	auto caZe= &CASE;
	if (cin.fail()) {
		cleanCin();
		return;
	}

	if((*caZe)==0 && cin.good()) {
		if(sku<0 || qty<0 || price<0) *caZe=1; //Negative
		if((sku>0 && sku<10000000) || (price>0 && price<0.5)) *caZe=2;	//Too Small
		if(qty>99 || sku>99999999 || price>20000) *caZe=3;	//Too Large
		switchBlade(*caZe);
	} else if ((*caZe)!=0 && (*caZe)!=111 ) switchBlade(*caZe); //directly invoke error message
	else if((menu<1 || menu>6) && (*caZe)==111) switchBlade(*caZe); //menu choice
};
